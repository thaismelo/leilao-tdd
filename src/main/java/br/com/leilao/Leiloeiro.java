package br.com.leilao;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.util.List;

public class Leiloeiro {
    private String nome;
    private Leilao leilao;

    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance() {
        List<Lance> lances = this.leilao.getLances();

        if(lances.isEmpty()){
            throw new RuntimeException("Ainda não existem lances...");
        }

        Lance lance = lances.get(0);
        for (Lance lanceFor : lances) {
            if (lance.getValor() < lanceFor.getValor()) {
                lance = lanceFor;
            }
        }

        return lance;
    }
}
