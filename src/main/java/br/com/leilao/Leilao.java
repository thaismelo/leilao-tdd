package br.com.leilao;

import java.util.List;

public class Leilao {

    private List<Lance> lances;

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public Leilao() {
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        lances = lances;
    }

    public Lance validarLance(Lance lance) {
        for (Lance lanceFor : this.lances) {
            if (lance.getValor() < lanceFor.getValor()) {
                throw new RuntimeException("Lance deve ser maior que anterior");
            }
        }

        return lance;
    }

    public Lance adicionarNovoLance(Lance lance){
        Lance lanceValidado = validarLance(lance);
        this.lances.add(lanceValidado);

        return lanceValidado;
    }

}
