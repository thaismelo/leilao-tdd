package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTeste {

    private Usuario usuario;
    private Lance lance;
    private Leilao leilao;
    private Leiloeiro leiloeiro;

    @BeforeEach
    public void setUp(){
        this.usuario = new Usuario(1, "Thais");
        this.lance = new Lance(usuario, 50.00);
        Lance lanceMenor = new Lance(usuario, 40.00);

        List<Lance> lances = new ArrayList<>();
        lances.add(lance);
        lances.add(lanceMenor);

        leilao = new Leilao(lances);

        leiloeiro = new Leiloeiro("João", leilao);
    }


    @Test
    public void testarRetornarMaiorLance() {
        Lance lance = leiloeiro.retornarMaiorLance();

        Assertions.assertSame(this.lance, lance);
    }

    @Test
    public void testarRetornarOMaiorListaVazia(){
        List<Lance> lanceArrayList = new ArrayList<>();
        Leilao leilaoObjeto = new Leilao();
        Leiloeiro leiloeiroObjeto = new Leiloeiro();

        leilaoObjeto.setLances(lanceArrayList);
        leiloeiroObjeto.setLeilao(leilaoObjeto);

        Assertions.assertThrows(RuntimeException.class, ()->{leiloeiroObjeto.retornarMaiorLance();});
    }

}
